#!/usr/bin/python3
# https://docs.python.org/3.6/library/subprocess.html#subprocess.Popen


# python3 12-popen-sql.py

# psql -qtA -F';' trainng  -c "select * from clientes;"

# docker run --rm --name psql -h psql -it edtasixm06/postgres
# su -l postgres
# /usr/bin/pg_ctl -D /var/lib/pgsql/data -l logfile start
# psql -qtA -F';' training  -c "select * from clientes;"

# psql -qtA -F';' -h 172.17.0.2 -U edtasixm06 training -c 'select * from oficinas;'


from subprocess import Popen, PIPE

command = [f"psql -qtA -F';' -h 172.17.0.2 -U edtasixm06 training -c \
        'select * from clientes;'"]
pipeData = Popen(command, stdout=PIPE, shell=True)

for line in pipeData.stdout:
    print(line.decode('utf-8'), end='')

exit(0)
