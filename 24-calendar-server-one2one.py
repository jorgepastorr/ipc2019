# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

# 24-calendar-server-one2one.py [-p port] [-a any]

# Calendar server amb un popen, el client es connecta i rep el calendari. El server
# tanca la connexió amb el client un cop contestat però continua escoltant noves connexions.
# El server ha de governar-se amb senyals que fan:

# sigusr1: llista de peers i plega.
# sigusr2: count de listpeer i plega.
# sigterm: llista de peers, count i plega.

# El server és un daemon que es queda en execució després de fer un fork del seu
# pare (que mor) i es governa amb senyals.
# Que sigui el servidor qui rep l'any com a argument és una tonteria, seria més lògic
# fer-ho en el client, però el diàleg client servidor queda per a una pràctica
# posterior. Aquí es vol practicar usar un arg en el popen.

# ---------------------------------

import sys,socket, os, signal
import argparse
from subprocess import Popen, PIPE

# Global
conexiones = []

def mypeers(signum, frame):
    global conexiones
    print(f"conexiones: {conexiones}")
    sys.exit(1)

def mycount(signum, frame):
    global conexiones
    print(f"Total conexiones: {len(conexiones)}")
    sys.exit(1)

def myshow(signum, frame):
    global conexiones
    print(f"conexiones: {conexiones}\nTotal: {len(conexiones)}")
    sys.exit(1)


pid = os.fork()

if pid != 0:
    print("Programa Pare", os.getpid(), pid)
    print("Hasta lugo")
    sys.exit(0)


print("PID: ", os.getpid())

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", type=int, dest="port", metavar="port",
                     default=50001, help="numero depuerto en el que escucha el server")
parser.add_argument("-a", "--any", type=str, dest="any",
                     default='', metavar="any",
                     help="anio a mostrar calendario")
args=parser.parse_args()

HOST = ''
PORT = args.port
ANY = args.any
command = [f"cal {ANY}"]

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))

signal.signal(signal.SIGUSR2,mypeers) # 12  muestra lista y cierra
signal.signal(signal.SIGUSR1,mycount) # 10  muestra cont y cierra
signal.signal(signal.SIGTERM,myshow) # 15   muestra lista, count y cierra

s.listen(1)
while True:

    conn, addr = s.accept()
    print("Connected by", addr)
    conexiones.append(addr)
    
    pipeData = Popen(command, stdout=PIPE, shell=True)
    for line in pipeData.stdout:
        conn.send(line)
    conn.close()

sys.exit(0)


