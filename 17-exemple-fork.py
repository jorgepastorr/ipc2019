#!/usr/bin/python3
# coding: utf-8

#  Jorge Pastor
#  m06 2020

# https://docs.python.org/3.6/library/signal.html

# python3 prog.py segundos

import sys, os
print("hol, començament")
print("PID pare: ", os.getpid())

# duplica el proceso ( programa) cuando el valor que retorna 
# es diferente a 0 es el original(padre)
# si es 0 es el hijo
pid = os.fork()


if pid != 0:
    os.wait()
    print("Programa Pare", os.getpid(), pid)
else:
    print("Programa fill", os.getpid(), pid)

print("Hasta lugo")
sys.exit(0)