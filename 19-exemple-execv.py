#!/usr/bin/python3
# coding: utf-8

#  Jorge Pastor
#  m06 2020

# https://docs.python.org/3/library/os.html

# python3 prog.py

# Usant el programa d'exemple fork fer que el procés fill (un while infinit) es
# governi amb senyals. Amb siguser1 mostra "hola radiola" i amb sigusr2 mostra
# "adeu andreu" i finalitza.

# Ídem anterior però ara el programa fill execula un “ls -la /”. Executa un nou
# procés carregat amb execv. Aprofitar per veure les fiferents variants de exec.

# v, l, lp, ve

import sys, os, signal

print("hol, començament")
print("PID pare: ", os.getpid())

# duplica el proceso ( programa) cuando el valor que retorna 
# es diferente a 0 es el original(padre)
# si es 0 es el hijo
pid = os.fork()


if pid != 0:
    print("Programa Pare", os.getpid(), pid)
    print("Hasta lugo")
    sys.exit(0)

print("Programa fill", os.getpid(), pid)

# Supstituye el proceso actual por un nuevo proceso ls
# os.execv("/usr/bin/ls",["/usr/bin/ls","-ls","/"])
# os.execl("/usr/bin/ls","/usr/bin/ls","-ls","/")

# Con p utiliza la vartiable PATH para encontrar el ejecutable
# os.execlp("ls","ls","-ls","/")

# Todo lo que no tiene la e ereda lentorno del proceso padre, si la e
# existe hay que definirlas
os.execv("/usr/bin/bash",["/usr/bin/bash","show.sh"])
# os.execve("/usr/bin/bash",["/usr/bin/bash","show.sh"],\
#     {"HOSTNAME":"caracol", "edad":"22"})


# Esto nunca se ejecuta
print("Hasta lugo")
sys.exit(0)