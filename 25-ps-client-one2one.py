# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 25-ps-client-one2one.py [-p port] server
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

# Els clients es connecten a un servidor, envien un informe consistent en fer
# "ps ax" i finalitzen la connexió. El servidor rep l'informe del client i
# el desa a disc. Cada informe es desa amb el format: ip-port-timestamt.log, on
# timestamp té el format AADDMM-HHMMSS.

import sys,socket
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", type=int, dest="port", metavar="port",
                     help="numero depuerto en el que escucha el server",
                     default=50001)
parser.add_argument("server", type=str, 
                     default='', metavar="server",
                     help="ip/host del servidor")
args=parser.parse_args()

HOST = args.server
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# conecta con el server
s.connect((HOST, PORT))

command = "ps "
pipeData = Popen(command, stdout=PIPE, shell=True)

for line in pipeData.stdout:
    s.send(line)

s.close()
sys.exit(0)