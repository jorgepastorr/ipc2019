#!/usr/bin/python3
# coding: utf-8

# https://docs.python.org/3.3/library/argparse.html?highlight=argparse
# https://docs.python.org/3.3/howto/argparse.html?highlight=argparse

# prog.py [-n 5 | 10 | 15 ] fitcher default 10

import argparse
import sys


parser = argparse.ArgumentParser()
parser.add_argument("-n", type=int, dest="numLines", metavar="lines",
                     default=10, choices=[5, 10, 15], help="numero de lineas a recorrer")
parser.add_argument("fitcher", type=str, 
                     default="/dev/stdin", metavar="file",
                     help="fichero a recorrer, por defecto stdin")
args=parser.parse_args()

print(parser)
print(args)

# MAXLIN=args.numlines
# fileIn=open(args.fitcher,"r")

# counter=0
# for line in fileIn:
#   counter+=1
#   print (line, end=" ")
#   if counter==MAXLIN: break

# fileIn.close()
# exit(0)



# --- version 2 ---

# parser = argparse.ArgumentParser()
# parser.add_argument("-n", type=int, dest="numLines", metavar="lines",
#                      default=10, choices=[5, 10, 15], help="numero de lineas a recorrer")
# parser.add_argument( "-f", "--file", dest="fitcher", type=str, required=True,
#                      default="/dev/stdin", metavar="file",
#                      help="fichero a recorrer, por defecto stdin")
# args=parser.parse_args()

# print(parser)
# print(args)

# MAXLIN=args.numlines
# fileIn=open(args.fitcher,"r")

# counter=0
# for line in fileIn:
#   counter+=1
#   print (line, end=" ")
#   if counter==MAXLIN: break

# fileIn.close()
# exit(0)
