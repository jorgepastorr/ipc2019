#!/usr/bin/python3
# coding: utf-8

#  Jorge Pastor
#  m06 2020

# https://docs.python.org/3/library/os.html

# python3 prog.py

# Usant l'exemple execv programar un procés pare que llança un fill i finalitza.
# El procés fill executa amb execv el programa python 16-signal.py al que li
# passa un valor hardcoded de segons.

import os, sys

print("hol, començament")
print("PID pare: ", os.getpid())

# duplica el proceso ( programa) cuando el valor que retorna 
# es diferente a 0 es el original(padre)
# si es 0 es el hijo
pid = os.fork()


if pid != 0:
    print("Programa Pare", os.getpid(), pid)
    print("Hasta lugo")
    sys.exit(0)

print("Programa fill", os.getpid())

os.execv("/usr/bin/python3",["/usr/bin/python3",\
    "16-signal.py", "100"])
