#!/usr/bin/python3
# coding: utf-8

# 10-count-by-group.py [ -s gid | gname | nusers ] users groups

# listar grupos
# cantidad de usuarios que pertenecen a un grupo
# * obligatorio no duplicados

# iterar grupos
# iterar usuarios

# https://docs.python.org/3.3/library/argparse.html?highlight=argparse
# https://docs.python.org/3.3/howto/argparse.html?highlight=argparse

import argparse

groupDict = {}

class UnixUser():
    """ Classe unix user: prototipo de /etc/passwd
    login:passwd:uid:gid:gecos:home:shell"""
    def __init__(self, lineaUser):
        "constructor objetos UnixUser"
        linea = lineaUser[:-1].split(':')
        self.login=linea[0]
        self.passwd=linea[1]
        self.uid=int(linea[2])
        self.gid=int(linea[3])
        self.gecos=linea[4]
        self.home=linea[5]
        self.shell=linea[6]
        # extraer gname del diccionario global
        self.gname=""
        if self.gid in groupDict:
            self.gname=groupDict[self.gid].gname
    def show(self):
        "Mostrar datos de usuario"
        print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gname:{self.gname} \
            gecos: {self.gecos} home: {self.home} shell: {self.shell}")
    def __str__(self):
        "Funcio per retornar string del objeto"
        return f"{self.login} {self.uid} {self.gid} {self.gname} {self.gecos}\
             {self.home} {self.shell}"

class UnixGroup():
    """Clase grup unix /etc/group"""
    def __init__(self, groupline):
        "constructor de un UNixGroup dada una linea"
        groupField = groupline[:-1].split(':')
        self.gname = groupField[0]
        self.passwd = groupField[1]
        self.gid = int(groupField[2])
        self.userList = []
        # solo anadir usuarios de grupo si existen
        if groupField[3] != '':
            self.userList = groupField[3].split(',')
    def __str__(self):
        "funcio string"
        return f"{self.gname} {self.passwd} {self.gid} {self.userList}"

parser = argparse.ArgumentParser()
parser.add_argument( "fileUsers", type=str, metavar="fileUsers",
                     help="fichero a recorrer (/etc/passwd)")
parser.add_argument( "fileGroups", type=str, metavar="fileGroups",
                     help="fichero a recorrer (/etc/group)")
parser.add_argument("-s", "--sort", type=str, dest="tipoOrden",
                     choices=['nusers', 'gid', 'gname'], metavar='nusers|gid|gname',
                     help="definir tipo de orden nusers|gid|gname")
args=parser.parse_args()

# Cargar grupos 
# guardar objetos en diccionario, para gestion de usuarios
archivo = open(args.fileGroups, 'r')
for line in archivo:
    oneGroup=UnixGroup(line)
    groupDict[oneGroup.gid] = oneGroup
archivo.close()

# Cargar usuarios
archivo = open(args.fileUsers, 'r')
for line in archivo:
    newUser = UnixUser(line)
    # cargar usuarios de grupos 
    if newUser.login not in groupDict[newUser.gid].userList:
        groupDict[newUser.gid].userList.append(newUser.login)
archivo.close()

# criterio de ordenacion
if args.tipoOrden == 'nusers' :
    index = [ (key, len(groupDict[key].userList), groupDict[key].gname ) for key in groupDict ]
    index.sort(key=lambda x: (x[1], x[2]))

elif args.tipoOrden == 'gid' :
    index = [ (key, groupDict[key].gid ) for key in groupDict ]
    index.sort(key=lambda x: x[1] )

else:
# elif args.tipoOrden == 'gname' :
    index = [ (key, groupDict[key].gname ) for key in groupDict ]
    index.sort(key=lambda x: str.lower(x[1]) )
    
# 4 mostrar datos
for key in index :
    print(groupDict[key[0]])