#!/usr/bin/python
#-*- coding: utf-8-*-
'''
# -----------------------------------------------------------------
# Escola del treball de Barcelona
# ASIX Hisi2 M06-ASO UF2NF1-Scripts
# @edt Curs 2019-2020
# Gener 2020
# Echo server multiple-connexions
# -----------------------------------------------------------------
'''
import socket, sys, select, os

HOST = ''                 
PORT = 50007             
# s es el socket que escucha
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
print(os.getpid())
# conexiones
conns=[s]
while True:
    # escucha por todos los elementos que estan en conns
    actius,x,y = select.select(conns,[],[])
    # actius es la lista de les conexions o sockets en que i ha activitat
    for actual in actius:
        if actual == s: # si es una nueva conexion
            conn, addr = s.accept()
            print('Connected by', addr)
            conns.append(conn)
        else: # es una conexion ya establecida
            data = actual.recv(1024)
            if not data:
                sys.stdout.write("Client finalitzat: %s \n" % (actual))
                actual.close()
                conns.remove(actual)
            else:
                actual.sendall(data)
                actual.sendall(bytes(chr(4), 'utf-8'),socket.MSG_DONTWAIT)
s.close()
sys.exit(0)


