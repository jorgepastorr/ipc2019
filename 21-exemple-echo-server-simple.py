# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind((HOST,PORT))
# activa la escucha
s.listen(1)
# empieza a escuchar hasta que un cliente se conecte
conn, addr = s.accept()
print("Connected by", addr)
while True:
  # escucha
  data = conn.recv(1024)
  # no dice, cuando no hay mas datos, dice que e a cerrado la conexion 
  # por el otro extremo
  # si han cerrado la conexion cierra
  if not data: break
  # envia
  conn.send(data)
conn.close()
sys.exit(0)


