#!/usr/bin/python3
# coding: utf-8

# https://docs.python.org/3.3/library/argparse.html?highlight=argparse
# https://docs.python.org/3.3/howto/argparse.html?highlight=argparse



class UnixUser():
    """ Classe unix user: prototipo de /etc/passwd
    login:passwd:uid:gid:gecos:home:shell"""
    
    def __init__(self, lineaUser):
        "constructor objetos UnixUser"
        linea = lineaUser.split(':')
        self.login=linea[0]
        self.passwd=linea[1]
        self.uid=linea[2]
        self.gid=linea[3]
        self.gecos=linea[4]
        self.home=linea[5]
        self.shell=linea[6]
    
    def show(self):
        "Mostrar datos de usuario"
        print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gecos: {self.gecos} home: {self.home} shell: {self.shell}")
    
    def changePasswd(self, passwd):
        self.passwd = passwd
    
    def changeHome(self, home):
        self.home = home

    def __str__(self):
        "Funcio per retornar string del objeto"
        return f"{self.login} {self.uid} {self.gid}"
        
# ejercicio 7 
# 07-list-users.py [-f file | default stdin] de /etc/passwd

# 1 populate cargar usuarios en una lista
# 2 recorrer esta lista y mostrarla



# coges una linea creas un objeto y lo añades a una lista

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-f", "--file", type=str, dest="fitcher",
                     default="/dev/stdin", metavar="file",
                     help="fichero a recorrer, por defecto stdin")
args=parser.parse_args()

print(args)

archivo = open(args.fitcher, 'r')
listUsers = []

# 1 populate cargar usuarios en una lista
for line in archivo:
    # quito el salto de linea si existe
    if line[-1:] == '\n' : line = line[:-1]
    listUsers.append( UnixUser(line) )

archivo.close()

# 2 recorrer esta lista y mostrarla
for user in listUsers:
    user.show()