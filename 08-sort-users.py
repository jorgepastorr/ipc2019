#!/usr/bin/python3
# coding: utf-8

# https://docs.python.org/3.3/library/argparse.html?highlight=argparse
# https://docs.python.org/3.3/howto/argparse.html?highlight=argparse

# criterio dev ordenacion: es que siempre salga en el mismo orden 
# independientemente de que hayan repetidos, 
# hay que hordenar por campo clave.

class UnixUser():
    """ Classe unix user: prototipo de /etc/passwd
    login:passwd:uid:gid:gecos:home:shell"""
    
    def __init__(self, lineaUser):
        "constructor objetos UnixUser"
        linea = lineaUser.split(':')
        self.login=linea[0]
        self.passwd=linea[1]
        self.uid=linea[2]
        self.gid=linea[3]
        self.gecos=linea[4]
        self.home=linea[5]
        self.shell=linea[6]
    
    def show(self):
        "Mostrar datos de usuario"
        print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gecos: {self.gecos} home: {self.home} shell: {self.shell}")
    
    def changePasswd(self, passwd):
        self.passwd = passwd
    
    def changeHome(self, home):
        self.home = home

    def __str__(self):
        "Funcio per retornar string del objeto"
        return f"{self.login} {self.uid} {self.gid}"
        
# ejercicio 8
# 07-sort-users.py [ -s login | gid ] file 

# 1 cargar datos, pasar linea
# 2 criterio de ordenacion por login o gid
# 3 mostrar datos

import argparse

parser = argparse.ArgumentParser()
parser.add_argument( "fileIn", type=str, 
                     default="/dev/stdin", metavar="file",
                     help="fichero a recorrerm en formato (/etc/passwd), por defecto stdin")
parser.add_argument("-s", "--sort", type=str, dest="tipoOrden",
                     choices=['login', 'gid'], metavar='login|gid',
                     help="definir tipo de orden login|gid")
args=parser.parse_args()

# print(args)

archivo = open(args.fileIn, 'r')
listUsers = []

# 1 cargar datos, pasar linea
for line in archivo:
    # quito el salto de linea si existe
    if line[-1:] == '\n' : line = line[:-1]
    listUsers.append( UnixUser(line) )

archivo.close()

# 2 criterio de ordenacion por login o gid
if args.tipoOrden == 'login' :
    listUsers = sorted( listUsers, key=lambda x: x.login )
if args.tipoOrden == 'gid' :
    listUsers = sorted( listUsers, key=lambda x: ( int(x.gid), x.login ) )

# 3 mostrar datos
for user in listUsers:
    print(user)
