# /usr/bin/python3
#-*- coding: utf-8-*-
# 
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

# server01.py [-p port] [-d debug]
# ip aws 35.177.14.0
# ---------------------------------

import sys,socket, os, signal
import argparse
from subprocess import Popen, PIPE

# Global
conexiones = []

def mypeers(signum, frame):
    global conexiones
    print(f"conexiones: {conexiones}")
    sys.exit(1)

def mycount(signum, frame):
    global conexiones
    print(f"Total conexiones: {len(conexiones)}")
    sys.exit(1)

def myshow(signum, frame):
    global conexiones
    print(f"conexiones: {conexiones}\nTotal: {len(conexiones)}")
    sys.exit(1)


pid = os.fork()

if pid != 0:
    print("Programa Pare", os.getpid(), pid)
    print("Hasta lugo")
    sys.exit(0)


print("PID: ", os.getpid())

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", type=int, dest="port", metavar="port",
                     default=44444, help="numero depuerto en el que escucha el server")
parser.add_argument("-d", "--debug", action='store_true',
                     help="mostrar debug")
args=parser.parse_args()

HOST = ''
PORT = args.port
DEBUG = args.debug

signal.signal(signal.SIGUSR2,mycount) # 12  muestra cont y cierra
signal.signal(signal.SIGUSR1,mypeers) # 10  muestra lista y cierra
signal.signal(signal.SIGTERM,myshow) # 15   muestra lista, count y cierra


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
s.listen(1)

MYEOF = bytes(chr(4), 'utf-8')

while True:

    conn, addr = s.accept()
    print("Connected by", addr)
    conexiones.append(addr)
    
    while True:
        # Espera a recivir una orden del cliente, para despues enviarle el resultado
        data = conn.recv(1024)
        
        if DEBUG: print('Recive', repr(data))

        if not data: break
        if b'processos' in data :
            data = 'ps ax'
        elif b'ports' in data:
            data = 'netstat -puta'
        else:
            data = 'uname -a'
        
        pipeData = Popen(data, stdout=PIPE, stderr=PIPE, shell=True)
        for line in pipeData.stdout:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line,'utf-8'))

        for line in pipeData.stderr:
            conn.sendall(line)
            if DEBUG: sys.stderr.write(str(line,'utf-8'))
        
        # Se establece fin de linea, si se quiere usar un cliente tipo script
        conn.sendall(MYEOF)

sys.exit(0)


