#!/usr/bin/python3
# coding: utf-8

# https://docs.python.org/3.3/library/argparse.html?highlight=argparse
# https://docs.python.org/3.3/howto/argparse.html?highlight=argparse

# prog.py [-n 5 | 10 | 15 ] [-f file]...

import argparse
import sys


# parser = argparse.ArgumentParser()
# parser.add_argument("-n", type=int, dest="numLines", metavar="lines",
#                      default=10, choices=[5, 10, 15], help="numero de lineas a recorrer")
# parser.add_argument("-f", "--file", type=str, action='append',
#                      default="/dev/stdin", metavar="file",
#                      help="fichero a recorrer, por defecto stdin",
#                      dest="fitcher")
# args=parser.parse_args()

# print(parser)
# print(args)


# parser = argparse.ArgumentParser()
# parser.add_argument("-n", type=int, dest="numLines", metavar="lines",
#                      default=10, choices=[5, 10, 15], help="numero de lineas a recorrer")
# parser.add_argument("-f", "--file", type=str, action='append',
#                      default="/dev/stdin", metavar="file",
#                      help="fichero a recorrer, por defecto stdin",
#                      dest="fitcher")
# parser.add_argument("-v", "--verbose", 
#                     help="increase output verbosity", action="store_true")
# args=parser.parse_args()

# print(parser)
# print(args)

parser = argparse.ArgumentParser()
parser.add_argument("-n", type=int, dest="numLines", metavar="lines",
                     default=10, choices=[5, 10, 15], help="numero de lineas a recorrer")
parser.add_argument("-f", "--file", type=str, action='append',
                     default="/dev/stdin", metavar="file",
                     help="fichero a recorrer, por defecto stdin",
                     dest="fitcher", nargs="+")
parser.add_argument("-v", "--verbose", 
                    help="increase output verbosity", action="store_true")
args=parser.parse_args()

print(parser)
print(args)


MAXLIN=args.numlines
ficheros = args.fitcher

def headFile(fitcher):
    fileIn=open(fitcher,"r")

    counter=0
    for line in fileIn:
        counter+=1
        print (line, end=" ")
        if counter==MAXLIN: break

    fileIn.close()

for fit in ficheros:
    if args.verbose : print(f"------vervose-------")
    headFile(fit)
