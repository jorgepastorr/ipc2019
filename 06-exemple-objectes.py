#!/usr/bin/python3
# coding: utf-8

# https://docs.python.org/3.3/library/argparse.html?highlight=argparse
# https://docs.python.org/3.3/howto/argparse.html?highlight=argparse


# clases tienen propiedades y metodos, traducido atributos y funciones.

class UnixUser():
    """ Classe unix user: prototipo de /etc/passwd
    login:passwd:uid:gid:gecos:home:shell"""
    
    def __init__(self, l, i, g):
        "constructor objetos UnixUser"
        self.login=l
        self.uid=i
        self.gid=g
    
    def show(self):
        "Mostrar datos de usuario"
        print(f"login: {self.login} uid: {self.uid} gid: {self.gid}")
    
    def sumaun(self):
        self.uid += 1
    
    def __str__(self):
        "Funcio per retornar string del objeto"
        return f"{self.login} {self.uid} {self.gid}"
        

# ejercicio 7 
# 07-list-users.py [-f file | default stdin] de /etc/passwd

# 1 populate cargar usuarios en una lista
# 2 recorrer esta lista y mostrarla


# coges una linea creas un objeto y lo añades a una lista