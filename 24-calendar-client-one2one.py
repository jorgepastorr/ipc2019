# /usr/bin/python3
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018

# 24-calendar-client-one2one.py [-s server] [-p port]

# -------------------------------------


import sys,socket
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", type=int, dest="port", metavar="port",
                     default=50001, help="numero depuerto en el que escucha el server")
parser.add_argument("-s", "--server", type=str, 
                     default='', metavar="server",
                     help="anio a mostrar calendario")
args=parser.parse_args()

SERVER = ''
if args.server:
    SERVER = args.server

PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

# conecta con el server
s.connect((SERVER, PORT))

# espera la respuesta
while True:
    data = s.recv(1024)
    if not data: break
    print('Received', repr(data))

s.close()
sys.exit(0)


