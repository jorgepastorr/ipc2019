# Examen shockets

Consiste en un programa python3 que escucha peticiones de clientes via `nc, telnet, socket, ...` y reenvia las peticiones de las ordenes `processos, ports, whoareyou` si se envia cualquier otra orden se recive whoareyou.



## Máquina amazon

Ip de maquina en amazon `35.177.14.0`.

## Programa

El programa se ejecuta para bifurcarse en un hijo que eeste se quedacen segundo plano y el padre muere.

El hijo se domina con las señanes `SIGUSR!, SIGUSR2, SIGTERM`, puede indicarse un modo ndebug con `-d, --debug`  por defecto se ejecuta en el puerto 44444, para cambiar `-p, --port`

```bash
python3 server01.py [-p|--port] [-d| --debug]
```

## Cliente

El cliente se puede conectar via nc, telnet o el script `26-telnet-client.py`

```bash
nc 35.177.14.0 4444
telnet 35.177.14.0 4444
python3 26-telnet-client.py -s 35.177.14.0 -p 44444
```
