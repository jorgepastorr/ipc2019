#!/usr/bin/python3
# coding: utf-8
#  Jorge Pastor
#  m06 2020

# https://docs.python.org/3.6/library/signal.html

# python3 prog.py segundos

# sigusr1 +1
# sigusr2 -1
# sighub reinicia
# sigterm no plega quanto falta
# sigint ^C nom plega ignora
# sigalarm mostrar up, down, plega


import sys, os, signal

segundosEntrados = int(sys.argv[1])
up = 0
down = 0

def myhandler(signum, frame):
    print("signal handler called with signal:", signum)
    print("hasta luego lucas")
    sys.exit(1)

def myreload(signum, frame):
    global segundosEntrados
    # segundosEntrados *= 60
    signal.alarm(segundosEntrados)

def myup(signum, frame):
    global up
    up += 1
    tiempo = signal.alarm(0)
    tiempo += 60
    signal.alarm(tiempo)

def mydown(signum, frame):
    global down
    down += 1
    tiempo = signal.alarm(0)
    if tiempo > 60:
        tiempo -= 60
    signal.alarm(tiempo)

def myshow(signum, frame):
    global down, up
    print(f"up: {up} down: {down}")
    sys.exit(1)

def mytemp(signum, frame):
    tiempo = signal.alarm(0)
    signal.alarm(tiempo)
    print(tiempo)

signal.signal(signal.SIGALRM,myshow) # 14   muestra up, down, plega
signal.signal(signal.SIGUSR2,mydown) # 12   reduce 60s
signal.signal(signal.SIGUSR1,myup) # 10     aumenta 60s
signal.signal(signal.SIGTERM,mytemp) # 15   muestra tiempo
signal.signal(signal.SIGHUP,myreload) # 1   establece tiempo inicial
signal.signal(signal.SIGINT,signal.SIG_IGN) # 2 na de na


signal.alarm(segundosEntrados)

print(os.getpid())
while True:
    pass

signal.alarm(0)
sys.exit(0)