#!/usr/bin/python3
# https://docs.python.org/3.6/library/subprocess.html#subprocess.Popen


# python3 prog.py 'select * from oficinas;'

# psql -qtA -F';' trainng  -c "select * from clientes;"

# docker run --rm --name psql -h psql -it edtasixm06/postgres
# su -l postgres
# /usr/bin/pg_ctl -D /var/lib/pgsql/data -l logfile start
# psql -qtA -F';' training  -c "select * from clientes;"

# psql -qtA -F';' -h 172.17.0.2 -U edtasixm06 training -c 'select * from oficinas;'

import sys, argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        "Exemple popen")
parser.add_argument("select", type=str,\
        help='select a executar')
args = parser.parse_args()

command = f"psql -qtA -F';' -h 172.17.0.2 -U edtasixm06 training "
# pipeData = Popen(command, stdout=PIPE, shell=True)
pipeData = Popen(command, stdin=PIPE, stdout=PIPE, shell=True, bufsize=0, universal_newlines=True)
pipeData.stdin.write(f"{args.select}\n\q\n")

for line in pipeData.stdout:
    print(pipeData.stdout.readline(), end="")

exit(0)
