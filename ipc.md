# ipc

# pipes

https://docs.python.org/3.6/library/subprocess.html#subprocess.Popen

Con popen se crean pipes en python, Popen ejecuta el comando y lo envia al PIPE, despues con la variable recojida recorres los datos obtenidos del pipe.

```python
from subprocess import Popen, PIPE

command = [f"psql -qtA -F';' -h 172.17.0.2 -U edtasixm06 training -c \
        'select * from clientes;'"]
# popen ejecuta el comando y envia resultadom al pipe
pipeData = Popen(command, stdout=PIPE, shell=True)

# muestras el resultado obtenido del pipe
for line in pipeData.stdout:
    print(line.decode('utf-8'), end='')
exit(0)
```



Activando la opción de stdin en popen podrás seguir añdiendo datos al pipe.

```python
import sys, argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        "Exemple popen")
parser.add_argument("select", type=str,\
        help='select a executar')
args = parser.parse_args()

command = f"psql -qtA -F';' -h 172.17.0.2 -U edtasixm06 training "
pipeData = Popen(command, stdin=PIPE, stdout=PIPE, shell=True, bufsize=0, universal_newlines=True)
pipeData.stdin.write(f"{args.select}\n\q\n")

for line in pipeData.stdout:
    print(pipeData.stdout.readline(), end="")

exit(0)
```

## signals

https://docs.python.org/3.6/library/signal.html

Señales del sistema en python, se puede modificar el comportamientos de las señales enviadas a un programa, redirigiendo el comportamiento a funciones propias o a otras señales del sistema.

```python
import sys, os, signal

def myhandler(signum, frame):
    print("signal handler called with signal:", signum)
    print("hasta luego lucas")
    sys.exit(1)

def mydeath(signum, frame):
    print("signal handler called with signal:", signum)
    print("no hem dona la gana de morir")

# redirige la señal sigalarm a la funcionmyhandler
signal.signal(signal.SIGALRM,myhandler)
signal.signal(signal.SIGUSR1,mydeath) 
# redirige sigterm a sig ignore
signal.signal(signal.SIGTERM,signal.SIG_IGN)

# llama a sigalarm en 60 segundos
signal.alarm(60)

print(os.getpid())
while True:
    pass

signal.alarm(0)
sys.exit(0)
```



## fork/exec

**fork** duplica el proceso (programa) desde la linea de `os.fork()` desde ese momento, en la ejecución se duplica el programa hasta el final, por eso en este escript se ejecuta una sola vez el codigo hasta `os.fork` y luego al duplicarse, se ejecutan 2 procesos el padre y el hijo, mostrando cada uno su print.

```python
import sys, os
print("hol, començament")
print("PID pare: ", os.getpid())

# duplica el proceso ( programa) cuando el valor que retorna 
# es diferente a 0 es el original(padre)
# si es 0 es el hijo
pid = os.fork()


if pid != 0:
    os.wait()
    print("Programa Pare", os.getpid(), pid)
else:
    print("Programa fill", os.getpid(), pid)

print("Hasta lugo")
sys.exit(0)
```

**Exec** substituye al proceso actual por uno nuevo que ejecutará lo que se le haya indicado en el exec.

https://docs.python.org/3/library/os.html

diferentes modos de exec:

- la `l`  indica que se pasaran los argumentos en ( ruta, arg0, arg1, ... )

- la `v` indica que los argumentos se pasan en una lista ( ruta, [args] )

- la `p` utiliza la variable PATH para encontrar el ejecutable

- la `e` es para añadir nuevas variables de entorno.

```python
import sys, os, signal

print("hol, començament")
print("PID pare: ", os.getpid())

pid = os.fork()
if pid != 0:
    print("Programa Pare", os.getpid(), pid)
    print("Hasta lugo")
    sys.exit(0)

print("Programa fill", os.getpid(), pid)

# Supstituye el proceso actual por un nuevo proceso ls
# os.execv("/usr/bin/ls",["/usr/bin/ls","-ls","/"])
# os.execl("/usr/bin/ls","/usr/bin/ls","-ls","/")

# Con p utiliza la vartiable PATH para encontrar el ejecutable
# os.execlp("ls","ls","-ls","/")

# Todo lo que no tiene la e ereda lentorno del proceso padre, si la e
# existe hay que definirlas
os.execv("/usr/bin/bash",["/usr/bin/bash","show.sh"])
# os.execve("/usr/bin/bash",["/usr/bin/bash","show.sh"],\
#     {"HOSTNAME":"caracol", "edad":"22"})

# Esto nunca se ejecuta
print("Hasta lugo")
sys.exit(0)
```



## socket

conexio 

iporigen:puertoorigen - ipdesti:puertodesti

socket origen                          socket desti

generalmente el puerto origen es un puerto dinamic

stream tcp 

tgram udp



herramientas para hacer pruevas:

- ncat localhost 50001

- telnet localhost 50001

- ncat -l 50001



server

```python
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))
# activa la escucha
s.listen(1)
# empieza a escuchar hasta que un cliente se conecte
conn, addr = s.accept()
print("Connected by", addr)
while True:
  # escucha
  data = conn.recv(1024)
  # no dice, cuando no hay mas datos, dice que se a cerrado la conexion 
  # por el otro extremo
  # si han cerrado la conexion cierra
  if not data: break
  # envia
  conn.send(data)
conn.close()
sys.exit(0)
```

cliente

```python
import sys,socket
HOST = ''
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# conecta con el server
s.connect((HOST, PORT))
s.send(b'Hello, world')
# espera la respuesta
data = s.recv(1024)
s.close()
print('Received', repr(data))
sys.exit(0)
```


