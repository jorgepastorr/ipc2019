#!/usr/bin/python3
# coding: utf-8

# https://docs.python.org/3.3/library/argparse.html?highlight=argparse
# https://docs.python.org/3.3/howto/argparse.html?highlight=argparse

import argparse

# parser = argparse.ArgumentParser()
# parser.add_argument("-e", type=int, dest="useredat")
# args=parser.parse_args()
# print(parser)
# print(args)
# parse_args verifica los argumentos 

# parser = argparse.ArgumentParser(description=\
#     "programa ejemplo de procesar argumentos", \
#         prog="02-argumemnt.py", epilog="adeu andreu")
# parser.add_argument("-e","--edat", type=int, dest="useredat")
# args=parser.parse_args()
# print(parser)
# print(args)

# parser = argparse.ArgumentParser(description=\
#     "programa ejemplo de procesar argumentos", \
#         prog="02-argumemnt.py", epilog="adeu andreu")
# parser.add_argument("-e","--edat", type=int, dest="useredat", help="edad a procesar")
# parser.add_argument("fit", type=str, metavar="fileIn", help="fichero a procesar")
# args=parser.parse_args()
# print(parser)
# print(args)

# parser = argparse.ArgumentParser(description=\
#     "programa ejemplo de procesar argumentos", \
#         prog="02-argumemnt.py", epilog="adeu andreu")
# parser.add_argument("-e","--edat", type=int, dest="useredat", help="edad a procesar")
# parser.add_argument("fit", type=str, metavar="fileIn", help="fichero a procesar")
# args=parser.parse_args()
# print(parser)
# print(args)
# print(args.fit, args.useredat)

parser = argparse.ArgumentParser(description=\
    "programa ejemplo de procesar argumentos", \
        prog="02-argumemnt.py", epilog="adeu andreu")
parser.add_argument("-e","--edat", type=int, dest="useredat", help="edad a procesar")
parser.add_argument("-f", "--fitcher", type=str, metavar="fitcher", help="fichero a procesar")
args=parser.parse_args()
print(parser)
print(args)
print(args.fitcher, args.useredat)


exit(0)