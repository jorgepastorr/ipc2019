# /usr/bin/python
#-*- coding: utf-8-*-
#
# exemple-echoClient.py  
# -------------------------------------
# @ edt ASIX M06 Curs 2018-2019
# Gener 2018
# -------------------------------------
import sys,socket
HOST = '3.8.4.25'
PORT = 50001
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
# conecta con el server
s.connect((HOST, PORT))

# espera la respuesta
while True:
    data = s.recv(1024)
    if not data: break
    print('Received', repr(data))

s.close()
sys.exit(0)


