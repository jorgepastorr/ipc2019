#!/usr/bin/python3
# coding: utf-8

# ejercicio 9
# 07-gname-users.py [ -s login | gid | gname ] -u fileusers -f filegroups 
#  ( ficheros obligatorios ) 

# https://docs.python.org/3.3/library/argparse.html?highlight=argparse
# https://docs.python.org/3.3/howto/argparse.html?highlight=argparse

# criterio dev ordenacion: es que siempre salga en el mismo orden 
# independientemente de que hayan repetidos, 
# hay que hordenar por campo clave.

import argparse

groupDict = {}

class UnixUser():
    """ Classe unix user: prototipo de /etc/passwd
    login:passwd:uid:gid:gecos:home:shell"""
    
    def __init__(self, lineaUser):
        "constructor objetos UnixUser"
        linea = lineaUser[:-1].split(':')

        self.login=linea[0]
        self.passwd=linea[1]
        self.uid=linea[2]
        self.gid=linea[3]
        self.gecos=linea[4]
        self.home=linea[5]
        self.shell=linea[6]
        self.gname=""
        if self.gid in groupDict:
            self.gname=groupDict[self.gid].gname
    
    def show(self):
        "Mostrar datos de usuario"
        print(f"login: {self.login} uid: {self.uid} gid: {self.gid} gname:{self.gname} gecos: {self.gecos} home: {self.home} shell: {self.shell}")

    def changePasswd(self, passwd):
        self.passwd = passwd
    
    def changeHome(self, home):
        self.home = home

    def __str__(self):
        "Funcio per retornar string del objeto"
        return f"{self.login} {self.uid} {self.gid} {self.gname}"


class UnixGroup():
    """Clase grup unix /etc/group"""
    def __init__(self, groupline):
        "constructor de un UNixGroup dada una linea"
        groupField = groupline[:-1].split(':')
        self.gname = groupField[0]
        self.passwd = groupField[1]
        self.gid = groupField[2]
        self.userList = groupField[3]
    def __str__(self):
        "funcio string"
        return f"{self.gname} {self.passwd} {self.gid} {self.userList}"


parser = argparse.ArgumentParser()
parser.add_argument( "-u", type=str, dest="fileUsers", metavar="fileUsers",
                     help="fichero a recorrer (/etc/passwd)",
                     required=True)
parser.add_argument( "-f", type=str, dest="fileGroups", metavar="fileGroups",
                     help="fichero a recorrer (/etc/group)",
                     required=True)
parser.add_argument("-s", "--sort", type=str, dest="tipoOrden",
                     choices=['login', 'gid', 'gname'], metavar='login|gid|gname',
                     help="definir tipo de orden login|gid|gname")
args=parser.parse_args()

# 1. recoger grupos
archivo = open(args.fileGroups, 'r')
for line in archivo:
    oneGroup=UnixGroup(line)
    groupDict[oneGroup.gid] = oneGroup
archivo.close()

# 2 cargar datos usuario
archivo = open(args.fileUsers, 'r')
listUsers = []
for line in archivo:
    listUsers.append( UnixUser(line ) )
archivo.close()

# 3 criterio de ordenacion por login o gid
if args.tipoOrden == 'login' :
    listUsers = sorted( listUsers, key=lambda x: x.login )
elif args.tipoOrden == 'gid' :
    listUsers = sorted( listUsers, key=lambda x: (int(x.gid), x.login) )
elif args.tipoOrden == 'gname' :
    listUsers = sorted( listUsers, key=lambda x: (x.gname, x.login) )

# 4 mostrar datos
for user in listUsers:
    print(user)
