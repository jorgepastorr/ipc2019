UF2 automatización

scripts



IPC Inter Proces Comunication

- pipes

- signals

- shockets

- shared memory (memoria compartida)

ldd muestra librerias dinamicas

```bash
       $ ldd /bin/ls
               linux-vdso.so.1 (0x00007ffcc3563000)
               libselinux.so.1 => /lib64/libselinux.so.1 (0x00007f87e5459000)
               libcap.so.2 => /lib64/libcap.so.2 (0x00007f87e5254000)
               libc.so.6 => /lib64/libc.so.6 (0x00007f87e4e92000)
               
```

ficheros abiertos del sistema

```bash
[isx47787241@i09 ipc2019]$ lsof | wc -l
148981
```

pstree -s 1457

porv defecto siempre hay tres flujos abiertos en el sistema
- 0 stdim
- 1 stdout
- 2 stderr

named pipes
mkfifo /tmp/dades
crear tuberia
prw-r--r--. 1 isx47787241 hisx2 0 ene 17 10:00 /tmp/dades
