#!/usr/bin/python3
# https://docs.python.org/3.6/library/subprocess.html#subprocess.Popen


# python3 prog.py -d database [-c numclie]...

# docker run --rm --name psql -h psql -it edtasixm06/postgres
# su -l postgres
# /usr/bin/pg_ctl -D /var/lib/pgsql/data -l logfile start
# psql -qtA -F';' training  -c "select * from clientes;"

# psql -qtA -F';' -h 172.17.0.2 -U edtasixm06 training -c 'select * from oficinas;'


import sys, argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(description=\
        "Exemple popen")
parser.add_argument("-d", "--database", required=True, type=str,\
        help='database')
parser.add_argument("-c", "--client", action="append", type=str,\
        help='clientes a busar')
args = parser.parse_args()

command = f"psql -qtA -F';' -h 172.17.0.2 -U edtasixm06 {args.database} "
# pipeData = Popen(command, stdout=PIPE, shell=True)
pipeData = Popen(command, stdin=PIPE, stdout=PIPE, shell=True, bufsize=0,\
         universal_newlines=True)


for clie in args.client:
        pipeData.stdin.write(f"select * from clientes where \
                num_clie = {clie};\n")
        print(pipeData.stdout.readline(), end="")

pipeData.stdin.write(f"\q\n")

exit(0)
