#!/usr/bin/python3
# coding: utf-8

# https://docs.python.org/3.3/library/argparse.html?highlight=argparse
# https://docs.python.org/3.3/howto/argparse.html?highlight=argparse

# `03-head-args.py [-n lines] [-f file]`defecto 10 lineas defecto stdin

import argparse
import sys

# -- version 1 simple --

# parser = argparse.ArgumentParser()
# parser.add_argument("-n", type=int, dest="numLines", metavar="lines",
#                      default=10, help="numero de lineas a recorrer")
# parser.add_argument("-f", type=str, dest="fileIn",
#                      default="/dev/stdin", metavar="file",
#                      help="fichero a recorrer, por defecto stdin")
# args=parser.parse_args()

# print(parser)
# print(args)


# -- version 2 --

parser = argparse.ArgumentParser()
parser.add_argument("-n", type=int, dest="numLines", metavar="lines",
                     default=10, help="numero de lineas a recorrer")
parser.add_argument("-f", type=argparse.FileType('r'), dest="fileIn",
                     default=sys.stdin, metavar="file",
                     help="fichero a recorrer, por defecto stdin")
args=parser.parse_args()

# print(parser)
# print(args)

counter=0
for line in args.fileIn:
    counter += 1
    print(line, end="")
    if counter == args.numLines: break

args.fileIn.close()
exit(0)



