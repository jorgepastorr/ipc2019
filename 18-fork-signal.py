#!/usr/bin/python3
# coding: utf-8

#  Jorge Pastor
#  m06 2020

# https://docs.python.org/3.6/library/signal.html

# python3 prog.py

# Usant el programa d'exemple fork fer que el procés fill (un while infinit) es
# governi amb senyals. Amb siguser1 mostra "hola radiola" i amb sigusr2 mostra
# "adeu andreu" i finalitza.

# padre muerte

# hijo
# usr1 hola radiola
# usr2 adeu andreu plegar

import sys, os, signal

def mykill(signum, frame):
    print("adeu andre")
    sys.exit(0)

def myhello(signum, frame):
    print("Hola radiola")



print("hol, començament")
print("PID pare: ", os.getpid())

# duplica el proceso ( programa) cuando el valor que retorna 
# es diferente a 0 es el original(padre)
# si es 0 es el hijo
pid = os.fork()


if pid != 0:
    print("Programa Pare", os.getpid(), pid)
    print("Hasta lugo")
    sys.exit(0)

print("Programa fill", os.getpid(), pid)
signal.signal(signal.SIGUSR2,mykill) # 12   plega
signal.signal(signal.SIGUSR1,myhello) # 10     saludo

while True:
    pass
