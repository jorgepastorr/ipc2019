# /usr/bin/python3
#-*- coding: utf-8-*-
#
# 25-ps-server-one2one.py [-p port]
# -------------------------------------
# @ edt ASIX M06 Curs 2019-2020
# Gener 2020
# -------------------------------------

# Els clients es connecten a un servidor, envien un informe consistent en fer
# "ps ax" i finalitzen la connexió. El servidor rep l'informe del client i
# el desa a disc. Cada informe es desa amb el format: ip-port-timestamt.log, on
# timestamp té el format AADDMM-HHMMSS.

import sys,socket, os
from subprocess import Popen, PIPE
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("-p", "--port", type=int, dest="port", metavar="port",
                     help="numero depuerto en el que escucha el server",
                     default=50001)
args=parser.parse_args()

HOST = ''
PORT = args.port

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST,PORT))

while True:
    # Escuchar conexiones
    s.listen(1)
    conn, addr = s.accept()
    print("Connected by", addr)

    # definir string de archivo log
    ip = addr[0]
    port = addr[1]
    cmd = os.popen('date +%y%d%m-%H%M%S')
    date = cmd.readline()
    
    # archivo a escrivir log
    fileLog = open(f"{ip}-{port}-{date}.log","w")
    
    # escrivir en log los datos entrantes
    while True:
        data = conn.recv(1024)
        if not data: break
        fileLog.write(str(data)+"\n")
    
    # cerrado de carchivo y conexion
    fileLog.close()
    conn.close()


sys.exit(0)

